package com.tristan.catalogue.config;

import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

public class LoadBalancerConfiguration {
    @Bean
    public ServiceInstanceListSupplier
    discoveryClientServiceInstanceListSupplier(
            ConfigurableApplicationContext context) {
       // log.info("Configuring Load balancer to prefer the same instance");
        return ServiceInstanceListSupplier.builder()
                .withDiscoveryClient()
                .withSameInstancePreference()
                .build(context);
    }
}