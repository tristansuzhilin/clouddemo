package com.tristan.catalogue.route;

import com.tristan.catalogue.model.AccountResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class CatalogueRouter extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("direct:getAccountRisk")
                .log("Start getAccountProcessor")
                .log("${headers}")
                .log("${body}")
                .circuitBreaker()
                .resilience4jConfiguration().timeoutEnabled(true).timeoutDuration(2000).end()
                .process("getAccountProcessor")
                .log("After getAccountProcessor")
                .log("${headers}")
                .log("${body}")
                .end();

        from("direct:getAccount")
                .log("${headers}")
                .log("${body}")
                .process("setTokenProcessor")
                .process("getAccountTestProcessor")
                .log("After getAccountTestProcessor")
                .log("${headers}")
                .log("${body}")
                .end();

        from("direct:getStockByRisk")
                .log("${headers}")
                .log("${body}")
                .process("getStockRiskProcessor")
                .log("After getStockRiskProcessor")
                .log("${headers}")
                .log("${body}")
                .end();

        from("direct:getStockByAccount")
                .log("Start getStockByAccount")
                .log("${headers}")
                .log("${body}")
                .to("direct:getAccountRisk")
                .log("After direct:getAccountRisk")
                .log("${headers}")
                .log("${body}")
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        AccountResponse accountResponse = exchange.getIn().getBody(AccountResponse.class);
                        exchange.getIn().setBody(accountResponse.getRiskAppetite());
                    }
                })
                .log("After getRiskAppetite from Account")
                .log("${headers}")
                .log("${body}")
                .choice()
                .when(body().isNotEqualTo(""))
                .to("direct:getStockByRisk")
                .otherwise()
                .end();

    }
}