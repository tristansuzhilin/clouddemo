package com.tristan.catalogue.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class RiskResponse {
    @JsonProperty("stockName")
    @Column(name = "stockName")
    private String stockName;

    @JsonProperty("risk")
    @Column(name = "risk")
    private String risk;
}
