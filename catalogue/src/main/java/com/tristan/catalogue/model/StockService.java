package com.tristan.catalogue.model;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="stock-producer")
public interface StockService {
    @RequestMapping(method = RequestMethod.GET, value = "/stock/getstock/{risk}")
    public ResponseEntity<List<RiskResponse>> getStock (@RequestHeader("from") String mService, @PathVariable String risk);
}
