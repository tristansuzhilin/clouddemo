package com.tristan.catalogue.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountResponse {
    @JsonProperty("name")
    @Column(name = "name")
    private String name;

    @JsonProperty("riskAppetite")
    @Column(name = "riskAppetite")
    private String riskAppetite;

    @JsonProperty("role")
    @Column(name = "role")
    private String role;
}