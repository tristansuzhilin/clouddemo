package com.tristan.catalogue.model;

import com.tristan.catalogue.config.LoadBalancerConfiguration;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name="account-producer")

@LoadBalancerClient(name="account-producer", configuration = LoadBalancerConfiguration.class)
public interface AccountService {
    @RequestMapping(method = RequestMethod.GET, value = "/account/getaccountrisk/{name}")
    public ResponseEntity<AccountResponse> getAccountRisk (@RequestHeader(value = "Authorization", required = true) String authorizationHeader,@PathVariable String name);

    @RequestMapping(method = RequestMethod.GET, value = "/account/")
    public ResponseEntity<String> getAccountTest (@RequestHeader(value = "Authorization", required = true) String authorizationHeader);
}
