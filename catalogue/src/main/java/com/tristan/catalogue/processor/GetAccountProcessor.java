package com.tristan.catalogue.processor;

import com.tristan.catalogue.model.AccountResponse;
import com.tristan.catalogue.model.AccountService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component("getAccountProcessor")
public class GetAccountProcessor implements Processor {

    @Autowired
    private AccountService accountService;

    Logger logger = LoggerFactory.getLogger(GetAccountProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        ResponseEntity<AccountResponse> responseEntity = accountService.getAccountRisk(exchange.getIn().getHeader("Authorization",String.class),exchange.getIn().getBody(String.class));
        AccountResponse accountResponse = responseEntity.getBody();
        Map<String, Object> headerMap = new HashMap<>();
        headerMap.put("Response", "AccountResponse" );
        exchange.getIn().setHeaders(headerMap);
        exchange.getIn().setBody(accountResponse);
    }
}