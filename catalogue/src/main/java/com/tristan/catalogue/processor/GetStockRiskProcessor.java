package com.tristan.catalogue.processor;

import com.tristan.catalogue.model.RiskResponse;
import com.tristan.catalogue.model.StockService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("getStockRiskProcessor")
public class GetStockRiskProcessor implements Processor {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private StockService stockService;

    Logger logger = LoggerFactory.getLogger(GetStockRiskProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        ResponseEntity<List<RiskResponse>> responseEntity = stockService.getStock("Catalogue",exchange.getIn().getBody(String.class));
        List<RiskResponse> riskResponse = responseEntity.getBody();
        Map<String, Object> headerMap = new HashMap<>();
        headerMap.put("Response", "RiskResponse" );
        exchange.getIn().setHeaders(headerMap);
        exchange.getIn().setBody(riskResponse);
    }
}
