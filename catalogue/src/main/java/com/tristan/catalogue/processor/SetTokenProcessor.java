package com.tristan.catalogue.processor;

import com.tristan.catalogue.model.RiskResponse;
import com.tristan.catalogue.model.StockService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("setTokenProcessor")
public class SetTokenProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, Object> headerMap = new HashMap<>();
        headerMap.put("Authorization", "Bearer "+exchange.getIn().getBody(String.class));
        exchange.getIn().setHeaders(headerMap);
    }
}
