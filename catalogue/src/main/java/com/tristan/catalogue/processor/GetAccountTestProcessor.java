package com.tristan.catalogue.processor;

import com.tristan.catalogue.model.AccountResponse;
import com.tristan.catalogue.model.AccountService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component("getAccountTestProcessor")
public class GetAccountTestProcessor  implements Processor {
    @Autowired
    private AccountService accountService;

    Logger logger = LoggerFactory.getLogger(GetAccountTestProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        ResponseEntity<String> responseEntity = accountService.getAccountTest(exchange.getIn().getHeader("Authorization",String.class));
        String accountResponse = responseEntity.getBody();
        Map<String, Object> headerMap = new HashMap<>();
        headerMap.put("Response", "AccountTestResponse" );
        exchange.getIn().setHeaders(headerMap);
        exchange.getIn().setBody(accountResponse);
    }
}
