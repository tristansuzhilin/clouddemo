package com.tristan.catalogue.controller;

import com.tristan.catalogue.model.AccountResponse;
import com.tristan.catalogue.model.RiskResponse;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;

import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/catalogue")
public class CatalogueController {

    @Autowired
    private ProducerTemplate template;

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping("/")
    public String sayHello(HttpServletRequest request,
                           HttpServletResponse response, Authentication authentication) {
        OAuth2AuthorizedClient authorizedClient = authorizedClientService.loadAuthorizedClient(((OAuth2AuthenticationToken) authentication).getAuthorizedClientRegistrationId(), authentication.getName());

        return "Hello "+ authorizedClient.getPrincipalName() +"World! Time on "+appName+" Controller REST is " + LocalDateTime.now()
                + "\n Token: "+authorizedClient.getAccessToken().getTokenType().getValue()+ " "+authorizedClient.getAccessToken().getTokenValue();
    }

    @GetMapping("/at")
    public String getAccountTest(@RegisteredOAuth2AuthorizedClient("login-client") OAuth2AuthorizedClient authorizedClient) {
         String accountResponse = template.requestBody("direct:getAccount",authorizedClient.getAccessToken().getTokenValue(),String.class);

        return accountResponse;
    }

    @RequestMapping(value = "/showaccount", method = { RequestMethod.POST }, consumes = "text/plain")
    public AccountResponse showAccount(@RegisteredOAuth2AuthorizedClient("login-client") OAuth2AuthorizedClient authorizedClient, @RequestBody String name ) {
        AccountResponse accountResponse = template.requestBodyAndHeader("direct:getAccountRisk",name,"Authorization",
                "Bearer "+authorizedClient.getAccessToken().getTokenValue(),AccountResponse.class);
        return accountResponse;
    }

    @RequestMapping(value = "/showriskstock", method = { RequestMethod.POST }, consumes = "text/plain")
    public List<RiskResponse> showStockByRisk(@RequestBody String risk ) {
        List<RiskResponse> riskResponse = template.requestBody("direct:getStockByRisk", risk, List.class);
        return riskResponse;
    }

    @GetMapping(value = "/showaccountstock/{name}")
    public List<RiskResponse> showStockByAccount(@RegisteredOAuth2AuthorizedClient("login-client") OAuth2AuthorizedClient authorizedClient, @PathVariable String name) {
        List<RiskResponse> riskResponse = template.requestBodyAndHeader("direct:getStockByAccount", name,"Authorization",
                "Bearer "+authorizedClient.getAccessToken().getTokenValue(), List.class);
        return riskResponse;
    }
}