# CloudDemo

## Contents
*   [Getting started](#Getting started)
*   [Catalogue](#Catalogue)
    * [Cat-Scope 1](#Cat-Scope 1)
    * [Cat-Scope 2](#Cat-Scope 2)
*   [Account](#Account)
    * [Acc-Scope 1](#Acc-Scope 1)
    * [Acc-Scope 2](#Acc-Scope 2)
*   [Stock](#Stock)
    * [Stk-Scope 1](#Stk-Scope 1)
    * [Stk-Scope 2](#Stk-Scope 2)
*   [Eureka Server](#Eureka Server)

## Getting started

This Demo shows a catalogue listing that uses account information to fetch stock data.

There are 4 Project used.
1. Account
2. Stock
3. Catalogue (Config Server)
4. Eureka Server

Account MicroService maintans all the user account information such as name,role and risk appetite.

Stock MicroService maintans all the stock information such as stock name, stock risk and listed by who.

Catalogue MicroService maintans the exchange of information between Account and Stock MicroService.

Eureka Server registers all above MicroService project as Eureka Clients.

Execution Seqeunce
Eureka Server -> Catalogue -> Account/Stock

## Catalogue

### Cat-Scope 1
Show account detail by **POST** webservices with the text body using test data name from Account.

> http://localhost:8888/catalogue/showaccount

```
Shawn
```
Show all stock risk by **POST** webservices with the text body using test data risk from Stock.

> http://localhost:8888/catalogue/showriskstock

```
Low

```
Show all stock risk based on account detail by **GET** webservices with the test data name from Account.

> http://localhost:8888/catalogue/showaccountstock/{name}

```
http://localhost:8888/catalogue/showaccountstock/Tristan

```

### Cat-Scope 2
Catalogue is setup as the config server. The following are the location where the properties files are stored. Update the *catalogue.message* value in the properties file to see the updates on the clients.

> catalogue/src/main/resources/config-files
> 
>> catalogue/src/main/resources/config-files/account-dev.properties
>> 
>> catalogue/src/main/resources/config-files/account-dev.properties

```
catalogue.message=Hello Stock MS with Refresh Messagess!
```

## Account

### Acc-Scope 1
The following are the test data available.

```
Tristan
John
Jane
Mary
Tom
Aaron
Emma

```
Create new account by **POST** webservices with the JSON body.

> http://localhost:8083/account/register

```
{
    "name": "Shawn",
    "riskAppetite": "Low",
    "role": "Business"
}
```
View account detail by **GET** webservices with the Header and test data for name below.

> http://localhost:8083/account/getaccountrisk/{name}

```
"from": "Catalogue"

```

### Acc-Scope 2
Account is setup as the client where it will fetch the config from Catalogue server. 

View the message from Catalogue server via the link below.

> http://localhost:8083/

Update the message from Catalogue server by **POST** webservices.

> http://localhost:8083/actuator/refresh

## Stock

### Stk-Scope 1
The following are the test data available.

```
High
Normal
Low

```
View list of stock based on risk by **GET** webservices with the Header and test data for risk below.

> http://localhost:8082/stock/getstock/{risk}

```
"from": "Catalogue"

```

### Stk-Scope 2
Stock is setup as the client where it will fetch the config from Catalogue server. 

View the message from Catalogue server via the link below.

> http://localhost:8082/

Update the message from Catalogue server by **POST** webservices.

> http://localhost:8082/actuator/refresh

## Eureka Server
Execute the Eureka Server first.

View the Instances currently registered with Eureka via the link below.

> http://localhost:8761/
