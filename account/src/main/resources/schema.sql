create sequence account_seq START WITH 500 INCREMENT BY 1;
create table account (id bigint not null, name varchar(255), risk_appetite varchar(255), role varchar(255), primary key (id));
