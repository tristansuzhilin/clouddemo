INSERT INTO account(id,name,risk_appetite,role) VALUES (NEXT VALUE FOR account_seq,'Tristan','High','Customer');
INSERT INTO account(id,name,risk_appetite,role) VALUES (NEXT VALUE FOR account_seq,'John','','Business');
INSERT INTO account(id,name,risk_appetite,role) VALUES (NEXT VALUE FOR account_seq,'Jane','Normal','Customer');
INSERT INTO account(id,name,risk_appetite,role) VALUES (NEXT VALUE FOR account_seq,'Mary','High','Customer');
INSERT INTO account(id,name,risk_appetite,role) VALUES (NEXT VALUE FOR account_seq,'Tom','','Business');
INSERT INTO account(id,name,risk_appetite,role) VALUES (NEXT VALUE FOR account_seq,'Aaron','Low','Customer');
INSERT INTO account(id,name,risk_appetite,role) VALUES (NEXT VALUE FOR account_seq,'Emma','High','Customer');