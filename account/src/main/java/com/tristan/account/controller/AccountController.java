package com.tristan.account.controller;

import com.tristan.account.model.Account;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RefreshScope
@RestController
@RequestMapping("/account")
public class AccountController {

    @Value("${config.message}")
    private String catalogueMessage;

    @Autowired
    private ProducerTemplate template;

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping("/")
    public String sayHello() {
        return "Hello World! Time on Account Producer Controller REST is" + LocalDateTime.now() +"\nMessage from config server: "+catalogueMessage;
    }

    @PostMapping(value="/register")
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        template.sendBody("direct:createAccount",account);
        return new ResponseEntity<>(account, HttpStatus.CREATED);
    }

    @GetMapping(value = "/getaccountrisk/{name}")
    public ResponseEntity<Account> getAccountRisk (@PathVariable String name) {
        Account account = template.requestBodyAndHeader("direct:getAccount",name,"from","Catalogue", Account.class);
        return new ResponseEntity<>(account, HttpStatus.OK);
    }
}