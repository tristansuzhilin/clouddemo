package com.tristan.account.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@Entity(name="Account")
@Table(name="ACCOUNT")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    @Id
    @JsonProperty("id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator="acc_seq_gen")
    @SequenceGenerator(name="acc_seq_gen", sequenceName="account_seq")
    @Column(name = "id")
    private long id;

    @JsonProperty("name")
    @Column(name = "name")
    private String name;

    @JsonProperty("role")
    @Column(name = "role")
    private String role;

    @JsonProperty("riskAppetite")
    @Column(name = "riskAppetite")
    private String riskAppetite;
}