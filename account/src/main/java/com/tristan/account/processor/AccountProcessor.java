package com.tristan.account.processor;

import com.tristan.account.model.Account;
import com.tristan.account.service.IAccountService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component("accountProcessor")
public class AccountProcessor implements Processor {
    @Autowired
    private IAccountService accountService;

    @Override
    public void process(Exchange exchange) throws Exception {

        String name = exchange.getIn().getBody(String.class);
        Account account = accountService.getAccountByName(name);

        if (account != null) {
            Map<String, Object> headerMap = new HashMap<>();
            headerMap.put("name", account.getName());
            exchange.getIn().setHeaders(headerMap);
            exchange.getIn().setBody(account);
        }
    }
}