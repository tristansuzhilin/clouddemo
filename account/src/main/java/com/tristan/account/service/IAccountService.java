package com.tristan.account.service;

import com.tristan.account.model.Account;

public interface IAccountService {
    public void createAccount(Account account);
    public Account getAccountByName(String name);
}
