package com.tristan.account.service;

import com.tristan.account.model.Account;
import com.tristan.account.repository.IAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("AccountService")
public class AccountService implements IAccountService{
    @Autowired
    private IAccountRepository accountRepository;

    @Override
    public void createAccount(Account account) {
        accountRepository.save(account);
    }

    @Override
    public Account getAccountByName(String name) {
        List<Account> accounts = accountRepository.findByName(name);
        return accounts.size() > 0 ? accounts.get(0) : null;
    }
}