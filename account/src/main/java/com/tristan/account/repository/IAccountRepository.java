package com.tristan.account.repository;

import com.tristan.account.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAccountRepository extends JpaRepository<Account,Long> {
    @Query(nativeQuery= true, value="SELECT * FROM Account WHERE name = ?1  ")
    List<Account> findByName(String name);
}