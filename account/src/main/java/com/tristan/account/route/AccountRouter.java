package com.tristan.account.route;

import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class AccountRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        Predicate validAccount = header("name").isNull();

        from("direct:createAccount")
                .log("Create New Account")
                .log("${headers}")
                .log("${body}")
                .bean("AccountService","createAccount");

        from("direct:getAccount")
                .log("Retrieve Account")
                .log("${headers}")
                .log("${body}")
                .choice()
                .when(header("from").isEqualTo("Catalogue"))
                .process("accountProcessor")
                .otherwise()
                .end()
                .log("After Account Processing")
                .log("${headers}")
                .log("${body}")
                .choice()
                .when(validAccount)
                .log("Account is not Valid.")
                .otherwise()
                .end();
    }
}