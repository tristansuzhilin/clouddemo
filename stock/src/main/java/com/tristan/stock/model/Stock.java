package com.tristan.stock.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@Entity(name="Stock")
@Table(name="STOCK")
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class Stock {
    @Id
    @JsonProperty("id")
    @GeneratedValue(strategy = GenerationType.AUTO, generator="stock_seq_gen")
    @SequenceGenerator(name="stock_seq_gen", sequenceName="stock_seq")
    @Column(name = "id")
    private long id;

    @JsonProperty("stockName")
    @Column(name = "stockName")
    private String stockName;

    @JsonProperty("risk")
    @Column(name = "risk")
    private String risk;

    @JsonProperty("listedBy")
    @Column(name = "listedBy")
    private String listedBy;

}
