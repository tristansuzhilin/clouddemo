package com.tristan.stock.repository;

import com.tristan.stock.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IStockRepository extends JpaRepository<Stock,Long> {
    @Query(nativeQuery= true, value="SELECT * FROM Stock WHERE risk = ?1")
    List<Stock> findByRisk(String risk);
}
