package com.tristan.stock.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class StockRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        from("direct:getRisk")
                .log("Retrieve Stock Risk")
                .log("${headers}")
                .log("${body}")
                .choice()
                .when(header("from").isEqualTo("Catalogue"))
                .process("stockProcessor")
                .otherwise()
                .end()
                .log("After stockProcessor Processing")
                .log("${headers}")
                .log("${body}")
                .end();
    }
}
