package com.tristan.stock.processor;

import com.tristan.stock.model.Stock;
import com.tristan.stock.service.IStockService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

@Component("stockProcessor")
public class StockProcessor implements Processor {
    @Autowired
    private IStockService stockService;

    @Override
    public void process(Exchange exchange) throws Exception {
        String risk = exchange.getIn().getBody(String.class);
        List<Stock> stocks = stockService.getStockByRisk(risk);

        if (stocks.size() > 0) {
            Map<String, Object> headerMap = new HashMap<>();
            headerMap.put("NumOfStock", stocks.size());
            exchange.getIn().setHeaders(headerMap);
            exchange.getIn().setBody(stocks,List.class);
        }
    }
}
