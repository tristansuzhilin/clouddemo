package com.tristan.stock.controller;

import com.tristan.stock.model.Stock;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RefreshScope
@RestController
@RequestMapping("/stock")
public class StockController {

    @Value("${config.message}")
    private String catalogueMessage;

    @Autowired
    private ProducerTemplate template;

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping("/")
    public String sayHello() {
        return "Hello World! Time on "+appName+" Controller REST is " + LocalDateTime.now() +"\nMessage from config server: "+catalogueMessage;
    }

    @GetMapping(value = "/getstock/{risk}")
    public ResponseEntity<List<Stock>> getStock (@RequestHeader("from") String mService, @PathVariable String risk) {
        List<Stock> stocks = template.requestBodyAndHeader("direct:getRisk",risk,"from",mService, List.class);
        return new ResponseEntity<>(stocks, HttpStatus.OK);
    }

}
