package com.tristan.stock.service;

import com.tristan.stock.model.Stock;
import com.tristan.stock.repository.IStockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("StockService")
public class StockService implements IStockService{

    @Autowired
    private IStockRepository stockRepository;

    @Override
    public void createStock(Stock stock) {
        stockRepository.save(stock);
    }

    @Override
    public List<Stock> getStockByRisk(String risk) {
        List<Stock> stocks = stockRepository.findByRisk(risk);
        return stocks.size() > 0 ? stocks : null;
    }
}
