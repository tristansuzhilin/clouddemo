package com.tristan.stock.service;

import com.tristan.stock.model.Stock;

import java.util.List;

public interface IStockService {
    public void createStock(Stock stock);
    public List<Stock> getStockByRisk(String risk);
}
