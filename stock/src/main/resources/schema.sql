create sequence stock_seq START WITH 500 INCREMENT BY 1;
create table stock (id bigint not null, listed_by varchar(255), risk varchar(255), stock_name varchar(255), primary key (id));
