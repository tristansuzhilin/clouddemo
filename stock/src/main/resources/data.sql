INSERT INTO stock(id,stock_name,risk,listed_by) VALUES (NEXT VALUE FOR stock_seq,'Company A','High','John');
INSERT INTO stock(id,stock_name,risk,listed_by) VALUES (NEXT VALUE FOR stock_seq,'Company B','Low','John');
INSERT INTO stock(id,stock_name,risk,listed_by) VALUES (NEXT VALUE FOR stock_seq,'Company C','Normal','John');
INSERT INTO stock(id,stock_name,risk,listed_by) VALUES (NEXT VALUE FOR stock_seq,'Company D','High','Tom');
INSERT INTO stock(id,stock_name,risk,listed_by) VALUES (NEXT VALUE FOR stock_seq,'Company E','Normal','John');
INSERT INTO stock(id,stock_name,risk,listed_by) VALUES (NEXT VALUE FOR stock_seq,'Company F','Low','John');
INSERT INTO stock(id,stock_name,risk,listed_by) VALUES (NEXT VALUE FOR stock_seq,'Company G','High','Tom');